// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _MillieMadness_H_
#define _MillieMadness_H_
#include "Arduino.h"
//add your includes for the project MillieMadness here
#include <Servo.h>

//end of add your includes here

//add your function definitions for the project MillieMadness here

int randomDelay();
int longDelay();
int shortDelay();
void setLaser(int, int);

void boomerang();
void creep();
void scan();
void taunt();
void wobble();
void zip();
void squiggle();
void blink();
void circle();
void zigzag();
void chase();
void hide();

const int laser = 3;

const int minX = 50;
const int maxX = 170;
const int minY = 75;
const int maxY = 120;
const int rangeX = maxX - minX;
const int rangeY = maxY - minY;
const float scaleX = rangeX / 180.0;
const float scaleY = rangeY / 180.0;

const float warpSpeed = 1.0;

const int BRIGHT = 10;
const int OFF = 0;

//Do not add code below this line
#endif /* _MillieMadness_H_ */
