/*
 * blink.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void blink() {
	float pause = randomDelay() + longDelay() + (20 / warpSpeed);

	for (int i = 0; i < 10; i++) {
		analogWrite(laser, OFF);
		delay(pause);
		analogWrite(laser, BRIGHT);
		delay(pause);
	}
}

