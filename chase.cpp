/*
 * chase.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void chase() {
	int randomSteps=random(1,5);
	int posX = random(18,45);
	int posY = random(18,45);
	setLaser(posX, posY);
	posX = 90;
	setLaser(posX, posY);
	for (int i = 40; i > 0; i--) {
		bool luck = random(0, 2); // Making our own luck
		if (luck) {
			posX += randomSteps * 2;
			setLaser(posX, posY);
		} else {
			posX -= randomSteps * 2;
			setLaser(posX, posY);
		}
		delay(longDelay());
	}

}

