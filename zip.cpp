/*
 * zip.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void zip() {
	for (int i = 0; i < 3; i++) {
		int posX = random(0, 180);
		int posY = random(0, 180);
		setLaser(posX, posY);
		delay(longDelay() * 10);
	}
}

