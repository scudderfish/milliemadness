# README #

This code is basically a refactoring of the code from http://www.instructables.com/id/CheetahBeam-a-DIY-Automatic-Cat-Laser-Toy/ by 
http://fluxaxiom.com/2017/01/cheetahbeam-a-laser-wielding-robot-for-your-cats/

The comment in the original code says 'Feel free to modify or use as you wish' so I'll interpret that as being a BSD-like licence.

It is built using Sloeber from http://eclipse.baeyens.it/

Milly (yes I spelt her name wrong when I created this) can be seen here https://youtu.be/z2iXcNNqNY4
