/*
 * squiggle.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void squiggle() {
	int posX = random(0, 180);
	int posY = random(0, 180);
	int randomSteps = random(1, 4);
	int randomSteps2 = random(1, 5);

	for (int i = 0; i < 120; i++) {
		if (i % 2) {
			posX += randomSteps2;
			posY += randomSteps;
		} else {
			posX -= randomSteps2;
			posY -= randomSteps;
		}
		setLaser(posX, posY);
		delay(randomSteps * 5 / warpSpeed);
	}

}

