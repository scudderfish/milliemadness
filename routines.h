/*
 * routines.h
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */

#ifndef ROUTINES_H_
#define ROUTINES_H_

void taunt();
void wobble();
void scan();
void zip();
void boomerang();
void creep();



#endif /* ROUTINES_H_ */
