/*
 * wobble.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void wobble() {
  int posX = random(0, 180);
  int posY = random(0, 180);
  int sPosX = random(0, 180);
  int sPosY = random(0, 180);
  int circleXcenter = random(45, 135);
  int circleYcenter = random(45, 135);
  int luck = random(0, 1);

  for (int rad = 5; rad < 15; rad++) {
    for (int i = 5; i > 0; i--) {
      float angle = i * 2 * 3.14 / 10;
      int lastPosX = posX;
      int lastPosY = posY;
      posX = circleXcenter + (cos(angle) * rad);
      posY = circleYcenter + (sin(angle) * rad);
      if (posX > lastPosX) { // Slow things down
        posX = lastPosX + 1;
      } else {
        posX = lastPosX - 1;
      }
      if (posY > lastPosY) {
        posY = lastPosY + 1;
      } else {
        posY = lastPosY - 1;
      }
      setLaser(posX, posY);

      if (luck) {
        if (sPosX > rangeX / 2) {
          circleXcenter -= 1;
        } else {
          circleXcenter += 1;
        }
      } else {
        if (sPosY > rangeY / 2) {
          circleYcenter -= 1;
        } else {
          circleYcenter += 1;
        }
      }
      delay(randomDelay() * 2);
    }
  }
}



