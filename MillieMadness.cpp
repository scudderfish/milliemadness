// Do not remove the include below
#include "MillieMadness.h"

Servo servoX;
Servo servoY;

int randomDelay() {
	return random(10, 51) / warpSpeed;
}
int longDelay() {
	return random(10, 101) / warpSpeed;
}
int shortDelay() {
	return random(20, 31) / warpSpeed;
}
int buffer[20];
bool eclipsePlotter = false;

void plot(int x, int y) {
	if (eclipsePlotter) {
		int pktSize;

		buffer[0] = 0xCDAB; //SimPlot packet header. Indicates start of data packet
		buffer[1] = 2 * sizeof(int); //Size of data in bytes. Does not include the header and size fields
		buffer[2] = x;
		buffer[3] = y;

		pktSize = 2 + 2 + (2 * sizeof(int)); //Header bytes + size field bytes + data

		//IMPORTANT: Change to serial port that is connected to PC
		Serial.write((uint8_t *) buffer, pktSize);
	} else {
		Serial.print(x);
		Serial.print("\t");
		Serial.println(y);

	}
}
void setLaser(int x, int y) {
	int scaledx = (x * scaleX + minX);
	int scaledy = (y * scaleY + minY);
	servoX.write(scaledx);
	servoY.write(scaledy);
	plot(x, y);
}

void setup() {
	Serial.begin(9600);
	pinMode(laser, OUTPUT);
	servoX.attach(4);
	servoY.attach(8);

	// Shuffle the random algorithm with an unconnected pin
	randomSeed(analogRead(0));
}

// The loop function is called in an endless loop
void loop() {

	int routine = 1; // random(0, 2);

	switch (routine) {
	case 0:
		taunt();
		break;
	case 1:
		wobble();
		break;
	case 2:
		scan();
		break;
	case 3:
		zip();
		break;
	case 4:
		boomerang();
		break;
	case 5:
		creep();
		break;
	case 6:
		squiggle();
		break;
	case 7:
		circle();
		break;
	case 8:
		zigzag();
		break;
	case 9:
		chase();
		break;
	case 10:
		hide();
		break;
	case 11:
		blink();
		break;
	}

}
