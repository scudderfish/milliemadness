/*
 * creep.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */

#include "MillieMadness.h"

void creep() {
	int posX = random(0, 180);
	int posY = random(0, 180);
	int luck = random(0,1);
	int randomSteps = random(1,4);

	int sPosX = 90;
	if (posX < sPosX) {
		for (; posX <= sPosX; posX++) {
			setLaser(posX, posY);
			delay(shortDelay());
		}
	} else {
		for (; posX >= sPosX; posX--) {
			setLaser(posX, posY);
			delay(shortDelay());
		}
	}
	int sPosY = 90;
	if (posY < sPosY) {
		for (; posY <= sPosY; posY++) {
			setLaser(posX, posY);
			delay(shortDelay());
		}
	} else {
		for (; posY >= sPosY; posY--) {
			setLaser(posX, posY);

			delay(shortDelay());
		}
	}
	for (int i = 0; i < 20; i++) {
		if (luck) {
			posX += randomSteps;
			posY += randomSteps;
		} else {
			posX -= randomSteps;
			posY -= randomSteps;
		}
		setLaser(posX, posY);
		delay(shortDelay() * 15);
	}
}

