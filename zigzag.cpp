/*
 * zigzag.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void zigzag() {
	bool luck = random(0, 2);
	int posX, posY;
	int randomSteps = random(1, 5);
	int pause = randomDelay();

	if (luck) {
		for (posX = 0; posX <= 18; posX += 1) {
			posY += randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posX = 18; posX <= 36; posX += 1) {
			posY -= randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posX = 36; posX <= 60; posX += 1) {
			posY += randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posX = 60; posX <= 72; posX += 1) {
			posY -= randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posX = 72; posX <= 90; posX += 1) {
			posY += randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posX = 90; posX <= 109; posX += 1) {
			posY -= randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
	} else {
		for (posY = 18; posY <= 36; posY++) {
			posX -= randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posY = 36; posY <= 60; posY++) {
			posX += randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posY = 60; posY <= 72; posY++) {
			posX -= randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}

		for (posY = 72; posY <= 90; posY++) {
			posX += randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posY = 90; posY <= 109; posY++) {
			posX -= randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}
		for (posY = 109; posY <= 127; posY++) {
			posX += randomSteps;
			setLaser(posX, posY);
			delay(pause);
		}

	}

}

