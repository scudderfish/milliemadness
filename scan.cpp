/*
 * scan.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void scan() {
  int pause = randomDelay();
  int posY = random(0, 180);
  for (int posX = 0; posX < 180; posX++) {
    setLaser(posX, posY);
    delay(pause);
  }
  for (int posX = 180; posX > 0; posX--) {
    setLaser(posX, posY);
    delay(pause);
  }
}



