/*
 * hide.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void hide() {
	bool luck=random(0,2);
	int pause=random(1,5)*1000/warpSpeed;
	if (luck) {
      analogWrite(laser,OFF); // Laser off
      delay(pause);
      analogWrite(laser,BRIGHT);
    }


}



