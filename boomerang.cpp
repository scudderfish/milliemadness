/*
 * boomerang.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void boomerang() {
	int posX = random(0, 180);
	int posY = random(0, 180);
	int luck = random(0, 2);

	int sPosX = random(0, 180);
	if (posX < sPosX) {
		for (; posX <= sPosX; posX++) {
			setLaser(posX, posY);
			delay(shortDelay());
		}
	} else {
		for (; posX >= sPosX; posX--) {
			setLaser(posX, posY);
			delay(shortDelay());
		}
	}
	for (; posY >= 0; posY--) {
		posY -= 1;
		setLaser(posX, posY);
		delay(shortDelay());
	}
	for (posY = 0; posY <= 144; posY += 1) {
		if (posY % 2) { // Wobble on the throw
			posX += 1;
			setLaser(posX, posY);
		} else {
			posX -= 1;
			setLaser(posX, posY);
		}
		setLaser(posX, posY);
		delay(randomDelay());
	}
	if (luck > 0) { // If we have no luck, the boomerang doesn't come back
		for (posY = 144; posY >= 0; posY -= 1) {
			if (posY > 90) { // Curve on return
				if (posY % 2) {
					posX += 1;
					setLaser(posX, posY);
				}
			} else {
				if (posY % 2) {
					posX -= 1;
					setLaser(posX, posY);
				}
			}
			setLaser(posX, posY);
			delay(longDelay());
		}
	}

}

