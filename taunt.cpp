/*
 * taunt.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"

void taunt() {
	for (int i = 0; i < 6; i++) {
		int posX = random(45, 135);
		int posY = random(45, 135);
		setLaser(posX, posY);
		delay(shortDelay() * 30);
	}

}
