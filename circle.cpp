/*
 * circle.cpp
 *
 *  Created on: 5 Mar 2017
 *      Author: dgs
 */
#include "MillieMadness.h"
void circle() {
	bool luck = random(0, 2);
	int posX = random(0, 180);
	int posY = random(0, 180);
	int circleXcenter = random(80, 101);
	int circleYcenter = random(80, 101);
	int pause = random(1, 5) * 6 / warpSpeed;

	if (luck) {
		for (int rad = 5; rad < 20; rad++) {
			for (int i = 0; i < 10; i++) {
				float angle = i * 2 * 3.14 / 10;
				posX = circleXcenter + (cos(angle) * rad);
				posY = circleYcenter + (sin(angle) * rad);
				setLaser(posX, posY);
				delay(pause);
			}
		}
	} else {
		for (int rad = 20; rad > 5; rad--) {
			for (int i = 10; i > 0; i--) {
				float angle = i * 2 * 3.14 / 10;
				posX = circleXcenter + (cos(angle) * rad);
				posY = circleYcenter + (sin(angle) * rad);
				setLaser(posX, posY);
				delay(pause);
			}
		}
	}

}

